package com.fearefull.todoreminder.ui.main;

public interface MainCaller {
    boolean reloadAlarmData();
}
