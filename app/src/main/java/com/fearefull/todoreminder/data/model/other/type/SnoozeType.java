package com.fearefull.todoreminder.data.model.other.type;

import java.io.Serializable;

public enum SnoozeType implements Serializable {
    NONE,
    FIRST,
    SECOND,
    THIRD,
}
